'''
Inverter
========

'''
import os
import pdb

from bag_ecd import bag_startup 
from bag_ecd.bag_design import bag_design

#This is mandatory
from inverter_gen.layout import layout

class inverter_gen(bag_design):

    @property
    def layout(self):
       ''' This property is used in bag_design as a handle to layout class of
       this generator. It provides access to e.g. layout parameters and is used 
       to provide the template for the layout template.

       OBS: It is mandatory to import the layout class in preamble as  

       .. code-block:: python 

           from <this_package>.layout import layout

       Returns 
       -------
       class
           Layout class 
       '''
       return layout


    # Define template draw and schematic parameters below 
    # property decorators:
    @property
    def ntap_w(self): 
        '''Width of the N substrate contact'''
        if not hasattr(self, '_ntap_w'):
            if self.finfet:
                self._ntap_w = 10
            else:
                self._ntap_w = 10 *self.min_lch
        return self._ntap_w
    @ntap_w.setter
    def ntap_w(self, val):
        self._ntap_w=val

    @property
    def ptap_w(self): 
        '''Width of P substrate contact'''
        if not hasattr(self, '_ptap_w'):
            if self.finfet:
                self._ptap_w = 10 
            else:
                self._ptap_w = 10 *self.min_lch
        return self._ptap_w

    @ptap_w.setter
    def ptap_w(self, val):
        self._ptap_w=val

    @property
    def lch(self): 
        '''Channel length of the transistors'''
        if not hasattr(self, '_lch'):
            self._lch=self.min_lch
        return self._lch
    @lch.setter
    def lch(self, val):
        self._lch=val

    @property
    def tran_intent(self): 
        '''Flavor of the transistor'''
        if not hasattr(self, '_tran_intent'):
            if self.finfet:
                self._tran_intent='standard'
            else:
                self._tran_intent='lvt'
        return self._tran_intent
    @tran_intent.setter
    def tran_intent(self, val):
        self._tran_intent=val

    @property
    def nmos_nf(self): 
        '''Number of fingers in NMOS transistor'''
        if not hasattr(self, '_nmos_nf'):
            self._nmos_nf=4
        return self._nmos_nf
    @nmos_nf.setter
    def nmos_nf(self, val):
        self._nmos_nf=val

    @property
    def nmos_w(self): 
        '''Width of the finger in NMOS transistor'''
        if not hasattr(self, '_nmos_w'):
            if self.finfet:
                self._nmos_w=10 
            else:
                self._nmos_w=10 *self.min_lch
        return self._nmos_w
    @nmos_w.setter
    def nmos_w(self, val):
        self._nmos_w=val

    @property
    def pmos_nf(self): 
        '''Number of fingers in PMOS transistor'''
        if not hasattr(self, '_pmos_nf'):
            self._pmos_nf=4
        return self._pmos_nf
    @pmos_nf.setter
    def pmos_nf(self, val):
        self._pmos_nf=val

    @property
    def pmos_w(self): 
        '''Width of PMOS transistor'''
        if not hasattr(self, '_pmos_w'):
            if self.finfet:
                self._pmos_w=10 
            else:
                self._pmos_w=10 *self.min_lch
        return self._pmos_w
    @pmos_w.setter
    def pmos_w(self, val):
        self._pmos_w=val

    @property
    def guard_ring_nf(self): 
        '''Width of guard ring'''
        if not hasattr(self, '_guard_ring_nf'):
            self._guard_ring_nf = 0 
        return self._guard_ring_nf
    @guard_ring_nf.setter
    def guard_ring_nf(self, val):
        self._guard_ring_nf=val

    @property
    def flip_well(self): 
        '''Enable Alternative structure for flipped well processes'''
        if not hasattr(self, '_flip_well'):
            self._flip_well = False
        return self._flip_well
    @flip_well.setter
    def flip_well(self, val):
        self._flip_well=val

    @property
    def finfet(self): 
        '''Enable finfet process'''
        if not hasattr(self, '_finfet'):
            self._finfet = False
        return self._finfet
    @finfet.setter
    def finfet(self, val):
        self._finfet=val


if __name__ == '__main__':
    import argparse
    from inverter_gen import inverter_gen
    
    # Implement argument parser
    parser = argparse.ArgumentParser(description='Parse selectors')
    parser.add_argument('--flip_well', dest='flip_well', type=bool, nargs='?', const = True, 
            default=False,help='Is flipwell process')
    parser.add_argument('--finfet',   dest='finfet', type=bool, nargs='?', const=True, 
            default=False, help='Is flipwell process')
    args=parser.parse_args()

    inst=inverter_gen()
    inst.print_log(type='I', msg='Generating with finfet =  %s' %(args.finfet))
    inst.print_log(type='I', msg='Generating with flip_well =  %s' %(args.flip_well))
    inst.flip_well = args.flip_well
    inst.finfet = args.finfet    
    inst.generate()

