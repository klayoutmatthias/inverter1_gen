#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.testbench_params import *


@dataclass
class inverter1_layout_params(LayoutParamsBase):
    """
    Parameter class for inverter1_gen

    Args:
    ----
    ntap_w : Union[float, int]
        Width of the N substrate contact

    ptap_w : Union[float, int]
        Width of P substrate contact

    lch : float
        Channel length of the transistors

    tran_intent : str
        NMOS/PMOS threshold flavor

    nmos_nf : int
        Number of fingers in NMOS transistor

    nmos_w : Union[float, int]
        Width of NMOS transistor

    pmos_nf : int
        Number of fingers in PMOS transistor

    pmos_w : Union[float, int]
        Width of PMOS transistor

    guard_ring_nf : int
        Width of guard ring

    flip_well : bool
        Enable Alternative structure for flipped well processes
    """

    ntap_w: Union[float, int]
    ptap_w: Union[float, int]
    lch: float
    tran_intent: str
    nmos_nf: int
    nmos_w: Union[float, int]
    pmos_nf: int
    pmos_w: Union[float, int]
    guard_ring_nf: int
    flip_well: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> inverter1_layout_params:
        return inverter1_layout_params(
            ntap_w=10,
            ptap_w=10,
            lch=min_lch,
            tran_intent='standard',
            nmos_nf=4,
            nmos_w=10,
            pmos_nf=4,
            pmos_w=10,
            guard_ring_nf=0,
            flip_well=False,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> inverter1_layout_params:
        return inverter1_layout_params(
            ntap_w=10 * min_lch,
            ptap_w=10 * min_lch,
            lch=min_lch,
            tran_intent='standard',
            nmos_nf=4,
            nmos_w=10 * min_lch,
            pmos_nf=4,
            pmos_w=10 * min_lch,
            guard_ring_nf=0,
            flip_well=False,
        )


@dataclass
class inverter1_params(GeneratorParamsBase):
    layout_parameters: inverter1_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> inverter1_params:
        return inverter1_params(
            layout_parameters=inverter1_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
