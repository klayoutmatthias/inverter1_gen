from typing import *

from abs_templates_ec.analog_core import AnalogBase
from bag.layout.util import BBox
from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase, TemplateDB

from .params import inverter1_layout_params


class layout(AnalogBase):
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='inverter1_layout_params parameter object',
        )

    def draw_layout(self):
        # A cheat sheet would be VERY handy!! 

        # Extract the params, just 
        # Reminder that these are defined when 
        # tbd_new_template is created

        params: inverter1_layout_params = self.params['params']

        show_pins = True

        # Calculate total number of fingers
        # Parameter manipulations goes here
        fg_tot = max(params.nmos_nf, params.pmos_nf) + 4
        ## Parameter manipulations end

        # Drawing parameters
        nw_list = [params.nmos_w]  # from bottom to up
        n_intent_list = [params.tran_intent]

        pw_list = [params.pmos_w]
        p_intent_list = [params.tran_intent]

        # Routing track definitions
        ng_tracks = [2]
        pg_tracks = [2]

        nds_tracks = [6]
        pds_tracks = [6]

        # n_orientations = ['R0']
        n_orientations = ['MX']
        p_orientations = ['R0']
        top_layer = 5

        # Create the AnalogBase template
        self.draw_base(params.lch, fg_tot, params.ptap_w, params.ntap_w, nw_list,
                       n_intent_list, pw_list, p_intent_list,
                       ng_tracks=ng_tracks, nds_tracks=nds_tracks,
                       pg_tracks=pg_tracks, pds_tracks=pds_tracks,
                       n_orientations=n_orientations, p_orientations=p_orientations, 
                       top_layer=top_layer,
                       guard_ring_nf=params.guard_ring_nf)

        # Drop connections to transistor
        # Direction codes 0 up 2 down
        # N-channel transistor at row 0, 
        # AnalogMosConn starting from col 0 (left) to col 5, 
        # Source direction 0 up drain direction 2 down
        #
        # OBS. 
        # R0 means that gates of the Nmos are below, MX above.
        # Thus, Source dir 0=up defines gate connections are aligned to source

        type = 'nch'
        row = 0
        start_col = int((fg_tot - params.nmos_nf)/2)
        source_dir = 2
        drain_dir = 0
        s_net = 'VSS'
        d_net = 'Q'
        ntran = self.draw_mos_conn(type, row, start_col, params.nmos_nf,
                                   source_dir, drain_dir, s_net=s_net, d_net=d_net)

        type = 'pch'
        row = 0
        start_col = int((fg_tot - params.pmos_nf)/2)
        source_dir = 0
        drain_dir = 2
        s_net = 'VDD'
        d_net = 'Q'
        ptran = self.draw_mos_conn(type, row, start_col, params.pmos_nf,
                                   source_dir, drain_dir, s_net=s_net, d_net=d_net )

        # Routing 
        #Source, drain and gate wires are already defined by Analog base

        # Track ID's define the routing type. Multiple types per track popssible
        # Gate routing
        tid_gate = self.make_track_id('nch', row_idx=0, tr_type='g', tr_idx=-2, width=1)
        gate_warr = self.connect_to_tracks([ntran['g'], ptran['g']], tid_gate)
        self.add_pin(self.get_pin_name('I'), gate_warr, show=show_pins)

        # N-drain horizontal routing
        didx = 0
        tid_ndrain = self.make_track_id('nch', row_idx=0, tr_type='ds', tr_idx=didx, width=1)
        ndrain_warr = self.connect_to_tracks(ntran['d'], tid_ndrain) 
        
        tid_pdrain=self.make_track_id('pch', row_idx=0, tr_type='ds', tr_idx=didx, width=1)

        # P-drain horizontal routing
        pdrain_warr = self.connect_to_tracks(ptran['d'], tid_pdrain)

        # P-source horizontal routing
        if params.flip_well:
            tid_psource = self.make_track_id('pch', row_idx=0, tr_type='ds', tr_idx=didx+3, width=4)
            psource_warr = self.connect_to_tracks(ptran['s'], tid_psource)

        # Location for vertical track
        loc = (ndrain_warr.get_bbox_array(self.grid).left + ndrain_warr.get_bbox_array(self.grid).right/2)
        idx = self.grid.coord_to_nearest_track(layer_id=5, coord=loc)

        # Vertical track to connect drains
        ver_id = TrackID(layer_id=5, track_idx=idx, width=1)
        out_warr = self.connect_to_tracks([ndrain_warr, pdrain_warr], ver_id)
        self.add_pin(self.get_pin_name('Q'), out_warr, show=show_pins)

        # Analog BAse has substrate definitions and a method to connect wirers 
        # to substrate
        self.connect_to_substrate('ptap', ntran['s'])
        if not params.flip_well:
            self.connect_to_substrate('ntap', ptran['s'])
        
        # Well connections and dummies
        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy(vss_width=6, vdd_width=6, lower=0, upper=self.bound_box.right)
        # Pins over wells
        if params.flip_well:
            self.add_pin(self.get_pin_name('VSS'), ptap_wire_arrs, label='VSS', show=True)
            self.add_pin(self.get_pin_name('VSS'), ntap_wire_arrs, label='VSS', show=True)
            self.add_pin(self.get_pin_name('VDD'), psource_warr, label='VDD', show=True)
        else:
            self.add_pin(self.get_pin_name('VDD'), ntap_wire_arrs, label='VDD', show=True)
            self.add_pin(self.get_pin_name('VSS'), ptap_wire_arrs, label='VSS', show=True)
        
        self.add_cell_boundary(self.bound_box)         
        self.set_size_from_bound_box(5, self.bound_box, round_up=True)
        # temp=self.extend_wires(self.get_all_port_pins('VSS'), lower=0, upper=200)

        # Create schematic parameters for update
        self._sch_params = {
            'params': params,
            'sch_dummy_info': self.get_sch_dummy_info(),
        }

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class inverter1(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)

