import os
from typing import *
from bag.design import Module

from .params import inverter1_layout_params


# The module path should be global. Is it?
yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/inverter1_templates',
                         'netlist_info', 'inverter1.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library inverter_templates cell inverter.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            params='inverter1_layout_params parameter object',
            sch_dummy_info='dummy data structure created by the layout generator',
        )

    def design(self,
               params: inverter1_layout_params,
               sch_dummy_info: List[Tuple[Any]]):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        self.instances['Xnmos'].design(w=params.nmos_w, l=params.lch, intent=params.tran_intent, nf=params.nmos_nf)
        self.instances['Xpmos'].design(w=params.pmos_w, l=params.lch, intent=params.tran_intent, nf=params.pmos_nf)
        if params.flip_well:
            self.reconnect_instance_terminal('Xpmos', 'B', 'VSS', index=None)
            self.design_dummy_transistors(sch_dummy_info, inst_name='Xdummy', vdd_name='VSS', vss_name='VSS')
        else:
            self.design_dummy_transistors(sch_dummy_info, inst_name='Xdummy', vdd_name='VDD', vss_name='VSS')

        
